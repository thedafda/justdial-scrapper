var request = require('request');
var jsdom = require("jsdom");
var _ = require('lodash');
var json2csv = require('json2csv');
var Promise = require("bluebird");
var fs = require('fs');

var fields = ['name', 'number', 'address' ,'coordinates'];
var scripts = [
	"http://code.jquery.com/jquery.js"
];

function getData(pageNum){
	return new Promise(function(resolve, reject){
		var url = "http://www.justdial.com/functions/ajxsearch.php?national_search=0&act=pagination&city=Hyderabad&search=Tyre+Puncture+Solutions&where=Malkajgiri&catid=1000720967thi&psearch=&prid=&page=" + pageNum;
		request(url, function (error, response, body) {
			if (!error && response.statusCode == 200) {
		    var json = JSON.parse(body);

			jsdom.env(json.markup, scripts, function (err, window) {
			    var $ = window.$;
			    var $body = $(window.document.body);
			    $(function(){
			    	console.log('Fetching... page:', pageNum);
			    	var data = [];

				    $('.jcar').map(function(indx, jcar){
				    	var name = $(jcar).find('.jcn').text();
				    	var mob = $(jcar).find('.jrcw').text();
				    	var address = $(jcar).find('.mrehover').text(); 
				    	var rsmap = $(jcar).find('.rsmap').attr('onclick') || '';
				    	name = $.trim(name);
				    	mob = $.trim(mob);
				    	address = $.trim(address);
				    	var rsmaptemp = rsmap.split(',');
				    	var coordinates = (rsmaptemp[3]|| '') + ',' + (rsmaptemp[4] || '');
				    	var  p = {
							name: name || '',
							number: mob || '',
							address: address || '',
							coordinates: coordinates || ''
						};
						data.push(p);
				    });

					console.log('Fetched page:', pageNum, 'total Records:', data.length);
				    resolve(data);
			    })
			});

		  } else {
		  	console.log('Error Whilte fetching ' +  url);
		  }
		});	
	});
}

var pArr = _.times(34, function(i){
	return getData(i);
});

Promise.all(pArr).then(function(data){

	console.log('Writing csv');

	var finalData = [];

	data.forEach(function(p){
		finalData = finalData.concat(p);
	});

	//console.log(finalData);

	json2csv({
		data: finalData,
		fields: fields,
		//del: '|'
	}, function(err, csv) {
		if (err){
			console.log(err);
			return;
		}
		fs.writeFile('file.csv', csv, function(err) {
			if (err) throw err;
			console.log('file saved');
		});
	});
});



